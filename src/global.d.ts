declare module 'my-module' {
    class SomeClass {
        static foo(a: string): void
    }
    export = SomeClass
}

